<section class="post-summary">
    <?php if ( has_post_thumbnail() ) { the_post_thumbnail('thumbnail'); } ?>
    <?php the_excerpt(); ?>
    <?php echo '<a class="link-to-post" href="' . get_the_permalink() .'" title="' . the_title_attribute(array( 'before' =>
    'Permalink to: ', 'after' => '', 'echo' => false )) .'" rel="bookmark">Read More
</section>
';?>
