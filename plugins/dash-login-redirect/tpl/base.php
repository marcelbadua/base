<?php

global $user_login;

if(isset($_GET['login']) && $_GET['login'] == 'failed')
{
    echo '<div class="error"><p>FAILED: Try again!</p></div>';
}

if (is_user_logged_in()) {
  echo '<div class="logout"> Hello, <div class="logout_user">', $user_login, '. You are already logged in.</div><a id="wp-submit" href="', wp_logout_url(), '" title="Logout">Logout</a></div>';
} else {
  $args = array(
    'echo'           => true,
    'redirect'       => home_url(),
    'form_id'        => 'loginform',
    'label_username' => __( 'Email Address' ),
    'label_password' => __( 'Password' ),
    'label_remember' => __( 'Remember Me' ),
    'label_log_in'   => __( 'Login' ),
    'id_username'    => 'user_login',
    'id_password'    => 'user_pass',
    'id_remember'    => 'rememberme',
    'id_submit'      => 'wp-submit',
    'remember'       => true,
    'value_username' => NULL,
    'value_remember' => true
  );
  wp_login_form($args);
}
