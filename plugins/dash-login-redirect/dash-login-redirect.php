<?php
/*
  Plugin Name:    Dash: Login Redirect
  Plugin URI: 
  Description:    Adds extra option for a post or page to redirect login page.
  Text Domain:    dash-login-redirect
  Version:        1.0
  Author:         Marcel Badua
  Author URI:     http://marcelbadua.com/
  License:        GNU General Public License v2
  License URI:    http://www.gnu.org/licenses/gpl-2.0.html
  */

if(!class_exists('DASH_LOGIN_REDIRECT'))
{
  class DASH_LOGIN_REDIRECT
  {
    /**
     * Construct the plugin object
     */
    public function __construct()
    {

      require_once(sprintf("%s/inc/functions.php", dirname(__FILE__)));
      $DASH_LOGIN_REDIRECT_FUNCTION = new DASH_LOGIN_REDIRECT_FUNCTION();

      require_once(sprintf("%s/inc/widget.php", dirname(__FILE__)));
      $DASH_LOGIN_REDIRECT_WIDGET = new DASH_LOGIN_REDIRECT_WIDGET();

    } // END public function __construct

    /**
     * Activate the plugin
     */
    public static function activate()
    {
      // Do nothing
    } // END public static function activate

    /**
     * Deactivate the plugin
     */
    public static function deactivate()
    {
      // Do nothing
    } // END public static function deactivate

    // Add the settings link to the plugins page
    function plugin_settings_link($links)
    {
      // $settings_link = '<a href="options-general.php?page=wp_plugin_template">Settings</a>';
      // array_unshift($links, $settings_link);
      // return $links;
    }


  } // END class DASH_LOGIN_REDIRECT
} // END if(!class_exists('DASH_LOGIN_REDIRECT'))

if(class_exists('DASH_LOGIN_REDIRECT'))
{
  // Installation and uninstallation hooks
  register_activation_hook(__FILE__, array('DASH_LOGIN_REDIRECT', 'activate'));
  register_deactivation_hook(__FILE__, array('DASH_LOGIN_REDIRECT', 'deactivate'));

  // instantiate the plugin class
  $wp_plugin_template = new DASH_LOGIN_REDIRECT();

}
