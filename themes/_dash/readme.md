#_dash Blank Theme Framework for Wordpress

https://bitbucket.org/marcelbadua/_dash

Dash is a blank theme framework for Wordpress. Better used with a child theme.

##Updates

###2.1.2
- disable wpemojicons on wordpress 4.8+

###2.1.0
- Extra Page details
- Github Updater

###2.0.0
- Site Information
- Built-in pagination and breadcrumb functions
- Widgets for sidebar and footer

------

If you like and using this theme, feel free to send me a hi, I would really appreciate that.

 - [Twitter](https://twitter.com/marcelbadua)
 - [marcelbadua](http://marcelbadua.com/)