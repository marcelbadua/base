<?php
foreach (glob(get_stylesheet_directory() . "/includes/*.php") as $file) {
  require $file;
}
add_action('wp_enqueue_scripts', 'theme_enqueue_styles');
function theme_enqueue_styles()
{
  wp_enqueue_style('fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', '', '4.7.0');
  wp_enqueue_script('_bourbon', get_stylesheet_directory_uri() . '/dist/js/scripts.min.js', array('jquery'), '1.0.0', true);
  wp_enqueue_style('_bourbon', get_stylesheet_directory_uri() . '/dist/css/styles.min.css', array(), '1.0.0', 'all');
  wp_enqueue_style('style', get_stylesheet_uri());
}
