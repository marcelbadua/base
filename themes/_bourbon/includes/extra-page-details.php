<?php
/**
 * Adds extra page details
 *
 * @package _dash
 */
class EXTRA_PAGE_DETAILS
{

  private $_meta = array(
    'page_id',
    'page_class',
  );

  public function __construct()
  {

    add_action('init', array(&$this, 'init'));

    add_action('admin_init', array(&$this, 'admin_init'));

  }

  public function init()
  {
    add_action('save_post', array(&$this, 'save_post'));

    add_filter('the_content', array(&$this, 'custom_content_class'));

  }

  public function custom_content_class($content)
  {

    global $post;

    $mcil_class = '<div id="' . @get_post_meta($post->ID, 'page_id', true) . '" class="' . @get_post_meta($post->ID, 'page_class', true) . '">';
    $mcil_class .= $content;
    $mcil_class .= '</div>';
    $filteredcontent = $mcil_class;
    return $filteredcontent;
  }

  /**
   * Save the metaboxes for this custom post type
   */
  public function save_post($post_id)
  {
    // verify if this is an auto save routine.
    // If it is our form has not been submitted, so we dont want to do anything
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
      return;
    }

    if (isset($_POST['post_type']) && $_POST['post_type'] == 'page' && current_user_can('edit_post', $post_id)) {
      foreach ($this->_meta as $field_name) {
        // Update the post's meta field
        update_post_meta($post_id, $field_name, $_POST[$field_name]);
      }
    } else {
      return;
    } // if($_POST['post_type'] == self::POST_TYPE && current_user_can('edit_post', $post_id))
  } // END public function save_post($post_id)
  public function admin_init()
  {
    // Add metaboxes
    add_action('add_meta_boxes', array(&$this, 'add_meta_boxes'));

  }

  /**
   * hook into WP's add_meta_boxes action hook
   */
  public function add_meta_boxes()
  {
    // Add this metabox to every selected post
    add_meta_box(
      'extra-page-details',
      'Extra Page Details',
      array(&$this, 'add_inner_meta_boxes'),
      array('page'),
      'side',
      'low'
    );
  } // END public function add_meta_boxes()
  /**
   * called off of the add meta box
   */
  public function add_inner_meta_boxes($post)
  {
    ?>

		<table>
		    <tr valign="top">
		        <th class="metabox_label_column">
		            <label for="page_id">Page ID</label>
		        </th>
		        <td>
		            <input type="text" id="page_id" name="page_id" value="<?php echo @get_post_meta($post->ID, 'page_id', true); ?>" />
		        </td>
		    </tr>
		    <tr valign="top">
		        <th class="metabox_label_column">
		            <label for="page_class">Page Class</label>
		        </th>
		        <td>
		            <input type="text" id="page_class" name="page_class" value="<?php echo @get_post_meta($post->ID, 'page_class', true); ?>" />
		        </td>
		    </tr>
		</table>

		<?php
} // END public function add_inner_meta_boxes($post)

}
// class EXTRA_PAGE_DETAILS
$EXTRA_PAGE_DETAILS = new EXTRA_PAGE_DETAILS();