<?php
if (!class_exists('DASH_LOGIN_REDIRECT_FUNCTION')) {
    class DASH_LOGIN_REDIRECT_FUNCTION
    {
        public function __construct()
        {
            add_action('init', array(&$this, 'init'));
            add_action('admin_init', array(&$this, 'admin_init'));
        }
        public function init()
        {
            add_action('template_redirect', array(&$this, 'redirect_user'));
            add_filter('login_redirect', array(&$this, 'my_login_redirect'), 10, 3);
            add_action('wp_logout', array(&$this, 'go_home'));
        }
        public function admin_init()
        {

        }

        public function redirect_user()
        {
            global $post;

            if ( (!is_user_logged_in()) && ($post->ID != 12) )
            {
                $return_url = esc_url( get_permalink(12) );//esc_url(home_url('/login/'));
                wp_redirect($return_url);
                exit;
            }
        }

        /* redirect users to front page after login */
        public function my_login_redirect($redirect_to, $request, $user)
        {
            //is there a user to check?
            global $user;
            if (isset($user->roles) && is_array($user->roles)) {
                //check for admins
                if (in_array('administrator', $user->roles)) {
                    // redirect them to the default place
                    return $redirect_to;
                } else {
                    return home_url();
                }
            } else {
                return $redirect_to;
            }
        }

        public function go_home()
        {
            $return_url = esc_url( get_permalink(12) );//esc_url(home_url('/login/'));
            wp_redirect($return_url);

            exit();
        }


    }
}
